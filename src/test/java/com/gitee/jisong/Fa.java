package com.gitee.jisong;

/**
 * @author wb.sunjisong18 2019-08-15
 */
public class Fa {

    public static void main(String[] args) {
        B b = new B();
        b.runFun();

    }
}


interface C{
    void runFun();
}


abstract class A implements C{
    abstract void fun();

    @Override
    public void runFun(){
        System.out.println("A runFun :" + this.getClass().getCanonicalName());
        fun();
    }
}



class B extends A implements C{

    @Override
    void fun() {
        System.out.println("B run :" + this.getClass().getCanonicalName());
    }
}