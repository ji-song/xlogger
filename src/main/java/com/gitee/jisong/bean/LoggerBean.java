package com.gitee.jisong.bean;

import com.gitee.jisong.enums.LogLevel;

import java.io.Serializable;

/**
 *
 * @author sunyinjie
 * @date 2017/10/24
 */
public class LoggerBean implements Serializable {
    private String name;
    /**
     * 日志等级 参照 LogLevelConstant
     */
    private LogLevel level;

    public LoggerBean() {
    }

    public LoggerBean(String name, LogLevel level) {
        this.name = name;
        this.level = level;
    }

    @Override
    public String toString() {
        return "{\"LoggerBean\":{"
                + "\"name\":\"" + name + "\""
                + ",\"level\":\"" + level + "\""
                + "}}";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LogLevel getLevel() {
        return level;
    }

    public void setLevel(LogLevel level) {
        this.level = level;
    }
}
