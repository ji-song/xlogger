package com.gitee.jisong.enums;

/**
 * @author sunjisong 2019-07-29
 */
public enum  LogLevel {

    OFF,
    FATAL,
    ERROR,
    WARN,
    INFO,
    DEBUG,
    TRACE,
    ALL;

    public static LogLevel getByDef(String text , LogLevel def){
        try{
            LogLevel logLevel = LogLevel.valueOf(text.toUpperCase());
            return null != logLevel ? logLevel : def;
        }catch ( Exception e){
            return def;
        }
    }
}
