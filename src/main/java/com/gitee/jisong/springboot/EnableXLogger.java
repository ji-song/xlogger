package com.gitee.jisong.springboot;

import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * @author sunjisong 2019-07-29
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({XLoggerConfig.class,XLoggerController.class})
public @interface EnableXLogger {
}
