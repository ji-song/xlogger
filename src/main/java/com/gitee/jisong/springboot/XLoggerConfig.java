package com.gitee.jisong.springboot;

import com.gitee.jisong.api.ChangeLogLevelProcess;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author sunjisong 2019-07-29
 */
@Configuration
public class XLoggerConfig {
    @Bean
    public ChangeLogLevelProcess changeLogLevelProcess(){
        return ChangeLogLevelProcess.getInstance();
    }
}
